
exports.accounts = [{
	id: 0,
	username: "Alice",
	password: "abc123"
}, {
	id: 1,
	username: "Bob",
	password: "abc123",
}, {
	id: 2,
	username: "Claire",
	password: "abc123"
}]


exports.playlists = [{
	id: 0,
	accountId: 0,
	title: "Alice's Favorites",
	isPublic: true
}, {
	id: 1,
	accountId: 0,
	title: "Dancing Music",
	isPublic: false
}, {
	id: 2,
	accountId: 0,
	title: "Christmas songs",
	isPublic: true
}, {
	id: 3,
	accountId: 1,
	title: "Bob's playlist",
	isPublic: false
}]

exports.songs = [{
	id: 0,
	playlistId: 0,
	title: "Calming Chills"
}, {
	id: 1,
	playlistId: 0,
	title: "Jars of Bees"
}, {
	id: 2,
	playlistId: 1,
	title: "Sea of Tranquility"
}, {
	id: 3,
	playlistId: 1,
	title: "Northern Lights"
}, {
	id: 4,
	playlistId: 1,
	title: "Ocean's song"
}, {
	id: 5,
	playlistId: 2,
	title: "Unwind"
}, {
	id: 6,
	playlistId: 2,
	title: "Diftwood"
}, {
	id: 7,
	playlistId: 2,
	title: "Bubble Bath"
}, {
	id: 8,
	playlistId: 2,
	title: "Avocado Morning"
}]