const {Router, response} = require('express');
const express = require('express');
const expressHandlebars = require('express-handlebars');
const bodyParser = require('body-parser');
const humanRouter = require('./routes/humansRouter');
const playlistRouter = require('./routes/playlistRouter');
const flash = require('connect-flash');
const expressSession = require('express-session');
const data = require('./data/dummy-data');
const db = require('./helpers/db.js');
// put this in in hopes to connect to playlist router
const create = require (".routes/playlistRouter");

//// attach this dummy dataset, calling the dataset somehow //


app.use {
  create 
}
//const data = require('./data/dummy-data')

const accounts = data.accounts;

//// this is middle ware that will give us access to sessions //

const app = express();

app.use(
  expressSession({
    secret: 'asfasfasfaskbkhoihbfa',
    saveUninitialized: false,
    resave: false,
  })
);

const correctUsername = 'Grace';
const correctPassword = 'hello';

app.engine(
  'hbs',
  expressHandlebars({
    defaultLayout: 'index.hbs',
  })
);

app.use(express.static('static'));

/// this code apparently retrives all dummy accounts 

app.get('/dummy-data', function (request, response) {
  db.all('SELECT ...', function (error, accounts) {
    const model = {
      allAccounts: accounts,
    };
    response.render('all-dummydata', model);
  });
});

//// middleware to make a log out, only when logged in ///

app.use(function (request, response, next) {
  const isLoggedIn = request.session.isLoggedIn;

  response.locals.isLoggedIn = isLoggedIn;

  next();
});

app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);

//// middleware function connect this a
app.use('/humans', humanRouter);

app.use('/playlists', playlistRouter);

///////////////// nav bar menu requests//////////////
/// I don't think I can put the links anywhere else?

app.get('/', function (request, response) {
  // Render /views/login.hbs
  const info = {
    data: data,
  };
  response.render('login.hbs', info);
});

app.get('/about', function (request, response) {
  // Render /views/home.hbs
  response.render('about.hbs');
});

app.get('/contact', function (request, response) {
  // Render /views/home.hbs
  response.render('contact.hbs');
});



app.get('/profile', function (request, response) {
  // Render /views/home.hbs
  const info = {
    data: data,
  };
  console.log('Profile page sucessful');
  response.render('profile.hbs', info);
});

app.get('/home', function (request, response) {
  // Render /views/home.hbs
  response.render('home.hbs');
});

app.get('/Deirdre', function (request, response) {
  // Render /views/Deirdre.hbs
  response.render('Deirdre.hbs');
});

app.get('/humans', function (request, response) {
  // Render /views/Deirdre.hbs
  response.render('humans.hbs');
});

app.get('/Users', function (request, response) {
  // Render /views/Deirdre.hbs
  response.render('Users.hbs', {layout: 'index2.hbs'});
});

app.get('/logout', function (request, response) {
  // Render /views/logout.hbs
  response.render('logout.hbs');
});

app.get('/private', function (request, response) {
  // Render /views/logout.hbs
  response.render('private.hbs', {layout: 'index2.hbs'});
});

app.get('/public', function (request, response) {
  // Render /views/logout.hbs
  response.render('public.hbs', {layout: 'index2.hbs'});
});

app.get('/sleepy', function (request, response) {
  // Render /views/logout.hbs
  response.render('sleepy.hbs',{layout: 'index2.hbs'});
 
});

app.get('/morningmed', function (request, response) {
  // Render /views/logout.hbs
  response.render('morningmed.hbs', {layout: 'index2.hbs'});
});

app.get('/instrum', function (request, response) {
  // Render /views/logout.hbs
  response.render('instrum.hbs', {layout: 'index2.hbs'});
});

app.get('/createplaylist', function (request, response) {
  // Render /views/logout.hbs
  response.render('createplaylist.hbs', {layout: 'index2.hbs'});
});

app.get('human', function (request, response) {
  // Render /views/home.hbs
  response.render('human.hbs', {layout: 'index2.hbs'});
});

///
app.get('/createhuman', function (request, response) {
  // Render /views/Deirdre.hbs
  response.render('create-human.hbs');
});


////////////////////database, dummy data, Tans code///

app.get('/dummy-data/:id', function (request, response) {
 const id = request.params.id;
const dummydata = dummydata.find((d) => d.id == id);
const model = {
 dummy,
 };

 //// response.render('login.bhs', model);

});


app.listen(8080);