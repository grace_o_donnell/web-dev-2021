var express = require('express');
var router = express.Router();
var db=require('../helpers/db');
// another routes also appear here
// this script to fetch data from MySQL databse table


router.get('/dummy-data', function(req, res, next) {
    var sql='SELECT * FROM dummy-data';
    db.query(sql, function (err, data, fields) {
    if (err) throw err;
    res.render('dummy-data', { title: 'User List', userData: data});
  });
});
module.exports = router;