const sqlite = require("sqlite3");

const db = new sqlite.Database("database.db");


db.run(`CREATE TABLE IF NOT EXISTS humans(
	id INTEGER PRIMARY KEY,
	name STRING,
	age INTEGER
)`);

db.run(`CREATE TABLE IF NOT EXISTS songs(
	id INTEGER PRIMARY KEY,
	playlistId INTEGER,
	title STRING
	 
)`);


exports.getAllHumans = function (callback) {
  const query = "SELECT * FROM humans ORDER BY id";

  db.all(query, function (error, humans) {
    callback(error, humans);
  });

  ////// end

  exports.createHuman = function (name, age, callback) {
    const query = "INSERT INTO humans (name, age) VALUES (?, ?)";
    const values = [name, age];

    db.run(query, values, function (error) {
      if (error) {
        callback("Database error.");
      } else {
        callback(null, this.lastID);
      }
    });
  };

  exports.getHumanById = function (humanId, callback) {
    const query = "SELECT * FROM humans WHERE id = ?";
    const values = [humanId];

    db.get(query, values, function (error, human) {
      if (error) {
        callback("Database error.");
      } else {
        callback(null, human);
      }
    });
  };
};

db.close();