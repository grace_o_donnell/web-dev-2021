const sqlite = require("sqlite3");

const db = new sqlite.Database("database.db");

db.run(`CREATE TABLE IF NOT EXISTS playlists(
	id INTEGER PRIMARY KEY,
	accountId INTEGER,
	title STRING
)`);  

/// GC ///
//// creating a playlist with these values?
// but why isn't it working, is it to do with the playlistRouter or here

exports.createPlaylist = function(accountId, song, callback) {
	const query = "INSERT INTO playlists (accountId, title) VALUES (?, ?)"
	const values = [accountId, song]
  
	db.run(query, values, function(error) {
	  callback(error, this.lastID)
	})
  }

  db.close();